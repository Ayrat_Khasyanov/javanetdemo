package ru.itis.kpfu;

import sun.lwawt.macosx.CSystemTray;

import java.io.InputStream;
import java.net.*;
import java.util.Scanner;

public class JavaNetDemo {
    public static void main(String[] args) {
        System.out.println("\n*** inetAddressDemo ***\n");
        inetAddressDemo();
        System.out.println("\n*** urlConnectionDemo ***\n");
        try {
            urlConnectionDemo();
        } catch (MalformedURLException e) {
            System.out.println(e.getCause());
        }

        String urlStr = "http://www.kpfu.ru";
        System.out.println("\n*** simpleBrowser for " + urlStr + " ***\n");
        simpleBrowser(urlStr);

        System.out.println("\n*** timeSocket ***\n");
        timeSocket();
    }

    private static void inetAddressDemo(){
        InetAddress addr;
        try {
            addr = InetAddress.getLocalHost();
            System.out.println(addr);
            addr = InetAddress.getByName("www.kpfu.ru");
            System.out.println(addr);
        } catch (Exception e) {
            System.err.println("JavaNetTest: exception:" + e);
        }
    }

    private static void urlConnectionDemo() throws MalformedURLException{
        URL aURL = new URL("http://java.sun.com:80/docs/books/" + "tutorial/index.html#DOWNLOADING");
        System.out.println("protocol = " + aURL.getProtocol());
        System.out.println("host = " + aURL.getHost());
        System.out.println("filename = " + aURL.getFile());
        System.out.println("port = " + aURL.getPort());
        System.out.println("ref = " + aURL.getRef());
    }

    private static void simpleBrowser(String urlStr){
        int c;
        URL url;
        try {
            url = new URL(urlStr);
            System.out.println("URL: " + url);
            URLConnection connection = url.openConnection();
            System.out.println("Host: " + url.getHost());
            System.out.println("Type: "+connection.getContentType());
            int len = connection.getContentLength();
            System.out.println("Content length= " + len);
            InputStream in = connection.getInputStream();
            while ((c = in.read()) != -1)
                System.out.print((char) c);
            in.close();
        } catch (Exception e) {
            System.err.println("URLConnectionTest: exception: " + e);
        }
    }

    private static void timeSocket(){
        try {
            Socket s=new Socket("time-A.timefreq.bldrdoc.gov", 13);
            try{
                Scanner in = new Scanner(s.getInputStream());
                while(in.hasNextLine())
                    System.out.println(in.nextLine());
            } finally {
                s.close();
            }
        } catch (Exception e) {
            System.out.println("TimeIs: exception: " + e);
        }
    }
}
